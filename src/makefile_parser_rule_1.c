#include <stdio.h>
#include <string.h>

#include "include/makefile_parser.h"
#include "include/tools.h"

int parse(FILE *file, struct rule_list *rule_list, struct var_list *var_list,
          size_t bytes)
{
    size_t curr_var = 0;
    size_t curr_rule = 0;

    for (int flag = 0; flag != -1; flag = flag)
    {
        char *curr_line = NULL;

        if ((flag = getline(&curr_line, &bytes, file)) == -1)
        {
            free(curr_line);
            break;
        }

        char *tmp = var_exists(curr_line, var_list);

        if (tmp == NULL)
            return 2;

        if (is_var(tmp) && tmp[0] != '\t')
        {
            size_t pos;

            if (!(pos = set_var_name(var_list->list[curr_var], tmp))
                || !set_var_val(var_list->list[curr_var++], tmp, pos))
                return 2;
        }

        else if (tmp != NULL && is_rule(tmp))
        {
            size_t pos = set_rule_name(rule_list->list[curr_rule], tmp);

            int has_dep = set_rule_dep(rule_list->list[curr_rule], tmp, pos);

            rule_list->list[curr_rule]->has_dep = has_dep;

            int has_recipe =
                set_rule_recipe(rule_list->list[curr_rule], file, tmp);

            rule_list->list[curr_rule++]->has_recipe = has_recipe;
        }

        free(tmp);
    }

    return 1;
}

struct rule_list *init_list(FILE *file)
{
    size_t bytes = 0;
    size_t total_rules = 0;

    int flag = 1;
    char *curr_line = NULL;

    while (flag != -1)
    {
        flag = getline(&curr_line, &bytes, file);

        if (flag == -1)
            break;

        if (is_rule(curr_line) && !is_var(curr_line))
            total_rules++;
    }

    free(curr_line);

    struct rule_list *rule_list = malloc(sizeof(struct rule_list));

    if (rule_list == NULL)
        return NULL;

    rule_list->size = total_rules;

    int init_res = init_rules(rule_list);

    if (!init_res)
        return NULL;

    return rule_list;
}

void destroy_list(struct rule_list *rule_list)
{
    for (size_t x = 0; x < rule_list->size; x++)
    {
        for (size_t i = 0; i < rule_list->list[x]->deps_num; i++)
        {
            free(rule_list->list[x]->deps[i]);
        }

        for (size_t i = 0; i < rule_list->list[x]->recp_num; i++)
        {
            free(rule_list->list[x]->recipes[i]);
        }

        free(rule_list->list[x]->rule_name);
        free(rule_list->list[x]);
    }

    free(rule_list);
}
