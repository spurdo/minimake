#include <stdio.h>
#include <string.h>

#include "include/makefile_parser.h"
#include "include/tools.h"

size_t set_rule_name(struct rule *rule, char *line)
{
    size_t i = 0;
    char buf[BUF_SIZE] = { 0 };

    for (i = 0; line[i] != ':'; i++)
    {
        buf[i] = line[i];
    }

    rule->rule_name = tab_to_char(buf, i);

    return i + 1;
}

int set_rule_dep(struct rule *rule, char *line, size_t pos)
{
    int first_space = 1;
    int not_flushed = 0;
    size_t buf_index = 0;
    size_t dep_index = 0;
    char dep_buffer[BUF_SIZE] = { 0 };

    if (line[pos + 1] == '\n' || line[pos] == '\n')
        return 0;

    while (line[pos] != '\n' && line[pos] != '\0' && line[pos] != '#')
    {
        if (line[pos] == '#')
            break;

        if (line[pos] != ' ' && line[pos] != '\t')
        {
            dep_buffer[buf_index++] = line[pos];
            first_space = 0;
            not_flushed = 1;
        }

        else if (!first_space)
        {
            rule->deps[dep_index++] = tab_to_char(dep_buffer, buf_index);
            memset(dep_buffer, 0, BUF_SIZE);
            buf_index = 0;
            not_flushed = 0;
        }

        pos++;
    }

    if (not_flushed)
        rule->deps[dep_index++] = tab_to_char(dep_buffer, buf_index);

    rule->deps_num = dep_index;

    return 1;
}

int set_rule_recipe(struct rule *rule, FILE *file, char *line)
{
    int has_recipe = 0;
    int is_first_line = 1;

    size_t cur_line = ftell(file);

    size_t bytes = 0;
    size_t rec_pos = 0;

    while (getline(&line, &bytes, file) != -1)
    {
        char rec_buffer[BUF_SIZE] = { 0 };

        if (!is_first_line && is_rule(line))
            break;

        if (is_recipe(line))
        {
            size_t pos = 0;
            has_recipe = 1;
            size_t buf_pos = 0;

            while (line[pos] == ' ' || line[pos] == '\t')
                pos++;

            for (size_t i = pos; line[i] != '\n'; i++)
            {
                if (line[i] != '\t')
                {
                    rec_buffer[buf_pos++] = line[i];
                }
            }

            rule->recipes[rec_pos++] = tab_to_char(rec_buffer, buf_pos);
        }

        is_first_line = 0;
    }

    fseek(file, cur_line, SEEK_SET);
    rule->recp_num = rec_pos;
    free(line);

    return has_recipe;
}

int init_rules(struct rule_list *rule_list)
{
    for (size_t i = 0; i < rule_list->size; i++)
    {
        rule_list->list[i] = malloc(sizeof(struct rule));

        if (rule_list->list[i] == NULL)
            return 0;

        rule_list->list[i]->has_dep = 1;
        rule_list->list[i]->has_recipe = 0;

        rule_list->list[i]->deps_num = 0;
        rule_list->list[i]->recp_num = 0;

        str_memset(rule_list->list[i]->deps);
        str_memset(rule_list->list[i]->recipes);

        rule_list->list[i]->rule_name = "";

        if (rule_list->list[i]->deps == NULL
            || rule_list->list[i]->recipes == NULL
            || rule_list->list[i]->rule_name == NULL)
        {
            free(rule_list->list[i]);
            return 0;
        }
    }

    return 1;
}