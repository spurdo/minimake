#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "include/command_parser.h"
#include "include/makefile_parser.h"
#include "include/shell.h"
#include "include/tools.h"

int start(int argc, char **argv, FILE *fp, char *filename)
{
    char *buf[BUF_SIZE];
    str_memset(buf);

    struct flag *flags = get_flags(argc, argv, buf, filename);
    struct rule_list *rule_list = init_list(fp);
    struct var_list *var_list = get_var_list(fp);

    if (rule_list == NULL || var_list == NULL || flags == NULL)
        return 2;

    int exec_res = exec_make(rule_list, var_list, flags, fp);

    destroy_list(rule_list);
    destroy_vars(var_list);
    free(flags);
    fclose(fp);

    if (exec_res == 2)
        errx(EXIT_FAILURE, "Syntax error!\n");

    if (exec_res == 1)
        exec_res = 2;

    return exec_res;
}

int main(int argc, char **argv)
{
    FILE *fp = NULL;

    char *filename = "";
    int file_pos = get_file_pos(argv);

    if (argc < 2 || file_pos == -1)
    {
        fp = fopen("makefile", "r");
        filename = "makefile";

        if (fp == NULL)
        {
            fp = fopen("Makefile", "r");
            filename = "Makefile";

            if (fp == NULL)
                errx(EXIT_FAILURE, "Could not find 'makefile' !");
        }
    }

    else
    {
        filename = argv[file_pos];
        fp = fopen(filename, "r");
    }

    if (fp == NULL)
    {
        printf("\nFile '%s' doesn't exist!\n", filename);
        return 2;
    }

    return start(argc, argv, fp, filename);
}
