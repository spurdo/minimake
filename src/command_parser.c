#define _GNU_SOURCE

#include "include/command_parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct flag *get_flags(int argc, char **argv, char **targets, char *filename)
{
    int opt = 0;
    int flags = 0;
    size_t i = 0;

    struct flag *flag = malloc(sizeof(struct flag));

    if (flag == NULL)
        return NULL;

    while ((opt = getopt(argc, argv, "fph")) != -1)
    {
        if (opt == 'f')
            flags |= FILE_FLAG;

        else if (opt == 'p')
            flags |= PRINT_FLAG;

        else if (opt == 'h' || opt == '?')
            flags |= HELP_FLAG;
    }

    while (argv[optind] != NULL)
    {
        if (strcmp(argv[optind], filename))
            targets[i++] = argv[optind];

        optind++;
    }

    flag->flags = flags;

    flag->targets = targets;

    return flag;
}
