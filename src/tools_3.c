#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/makefile_parser.h"
#include "include/tools.h"

static void treat(char *var_name, struct var_list *var_list, char *buf,
                  size_t *buf_pos)
{
    if (var_name[0] != '$')
    {
        char *var_val = get_val(var_name, var_list);

        if (var_val != NULL)
        {
            for (size_t x = 0; var_val[x] != '\0'; x++)
            {
                buf[*buf_pos] = var_val[x];
                *buf_pos += 1;
            }
        }
    }

    else
    {
        buf[*buf_pos] = '$';
        *buf_pos += 1;
    }
}

char *get_val(char *var, struct var_list *var_list)
{
    for (size_t i = 0; i < var_list->size; i++)
    {
        if (!strcmp(var, var_list->list[i]->name))
            return var_list->list[i]->val;
    }
    return NULL;
}

char *replace_with_val(char *line, struct var_list *var_list)
{
    size_t buf_pos = 0;

    char buf[BUF_SIZE] = { 0 };

    for (size_t i = 0; line[i] != '\0'; i = i)
    {
        size_t var_pos = 0;
        char var_name[BUF_SIZE] = { 0 };

        if (line[i] == '$')
        {
            size_t end = i;

            if (line[i + 1] == '$'
                || (line[i + 1] != '(' && line[i + 1] != '{'))
                var_name[var_pos++] = line[++end];

            else
            {
                end += 2;

                while (line[end] != '\0' && line[end] != ')'
                       && line[end] != '}')
                    var_name[var_pos++] = line[end++];
            }

            treat(var_name, var_list, buf, &buf_pos);

            i = end + 1;
        }

        else
            buf[buf_pos++] = line[i++];
    }

    return tab_to_char(buf, buf_pos);
}

void print_targets(char **buf)
{
    printf("\n# targets\n");
    for (size_t i = 0; buf[i] != NULL; i++)
        printf("[%s]\n", buf[i]);
}

int get_file_pos(char **argv)
{
    int i = 1;

    for (i = 0; argv[i] != NULL; i++)
        if (!strcmp(argv[i], "-f"))
            return i + 1;

    return -1;
}

char *replace_var_val(char *line, char *val, size_t val_len, size_t start)
{
    size_t end = start;
    size_t new_line_pos = 0;

    if (line[end + 1] == '$' || (line[end + 1] != '(' && line[end + 1] != '{'))
        end = end + 1;

    else
    {
        while (line[end] != ')' && line[end] != '}')
            end++;
    }

    char new_string[BUF_SIZE] = { 0 };

    for (size_t i = 0; line[i] != '\0'; i++)
    {
        if (i == start)
        {
            for (size_t x = 0; x < val_len; x++)
                new_string[new_line_pos++] = val[x];

            i = end;
        }
        else
            new_string[new_line_pos++] = line[i];
    }

    free(line);
    return tab_to_char(new_string, new_line_pos);
}