#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/makefile_parser.h"
#include "include/tools.h"

struct rule *get_rule(struct rule_list *list, char *name)
{
    size_t i;

    struct rule *r = NULL;

    for (i = 0; i < list->size; i++)
    {
        if (!(strcmp(list->list[i]->rule_name, name)))
            r = list->list[i];
    }

    return r;
}

void print_help(void)
{
    FILE *fp = fopen("src/.help", "r");

    if (fp == NULL)
        return;

    size_t bytes = 0;
    char *line = NULL;

    while (getline(&line, &bytes, fp) != -1)
    {
        printf("%s", line);
    }

    free(line);
    fclose(fp);
}

void print_vars(struct var_list *var_list)
{
    printf("# variables");
    for (size_t i = 0; i < var_list->size; i++)
    {
        printf("\n'%s' = ", var_list->list[i]->name);
        if (var_list->list[i]->val != NULL)
            printf("'%s'", var_list->list[i]->val);
    }
}

void print_data(struct rule_list *rule_list)
{
    printf("\n# rules\n");
    for (size_t i = 0; i < rule_list->size; i++)
    {
        printf("(%s):", rule_list->list[i]->rule_name);

        for (size_t x = 0; x < rule_list->list[i]->deps_num; x++)
        {
            if (rule_list->list[i]->deps[x] != NULL)
                printf(" [%s]", rule_list->list[i]->deps[x]);
        }

        printf("\n");

        for (size_t x = 0; x < rule_list->list[i]->recp_num; x++)
        {
            printf("\t'%s'\n", rule_list->list[i]->recipes[x]);
        }
    }
}

void str_memset(char *buf[])
{
    for (size_t i = 0; i < BUF_SIZE; i++)
    {
        buf[i] = NULL;
    }
}