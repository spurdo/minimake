#include <stdio.h>

#include "include/makefile_parser.h"
#include "include/tools.h"

int init_var(struct var_list *var_list)
{
    for (size_t i = 0; i < var_list->size; i++)
    {
        var_list->list[i] = malloc(sizeof(struct vars));

        if (var_list->list[i] == NULL)
            return 0;

        var_list->list[i]->name = "";
        var_list->list[i]->val = "";
    }

    return 1;
}

struct var_list *get_var_list(FILE *file)
{
    fseek(file, 0, SEEK_SET);

    size_t bytes = 0;
    size_t total_vars = 0;

    int flag = 1;
    char *curr_line = NULL;

    while (flag != -1)
    {
        flag = getline(&curr_line, &bytes, file);

        if (flag == -1)
            break;

        if (is_var(curr_line) && curr_line[0] != '\t')
            total_vars++;
    }

    free(curr_line);

    struct var_list *var_list = malloc(sizeof(struct var_list));

    if (var_list == NULL)
        return NULL;

    var_list->size = total_vars;

    int var_res = init_var(var_list);

    if (!var_res)
        return NULL;

    return var_list;
}

size_t set_var_name(struct vars *var, char *line)
{
    size_t pos = 0;
    size_t buf_pos = 0;
    char buf[BUF_SIZE] = { 0 };

    while (line[pos] != '#' && line[pos] != '\0'
           && (line[pos] == ' ' || line[pos] == '\t'))
    {
        pos++;
    }

    while (line[pos] != '\0' && line[pos] != '\n' && line[pos] != '=')
    {
        buf[buf_pos++] = line[pos];
        pos++;
    }

    if (line[pos + 1] != '=' && line[pos] != '=')
        return 0;

    buf_pos = 0;

    while (buf[buf_pos] != '\0' && buf[buf_pos] != ' ')
        buf_pos++;

    buf[buf_pos] = '\0';

    var->name = tab_to_char(buf, pos);
    var->name_len = buf_pos;

    return pos + 1;
}

int set_var_val(struct vars *var, char *line, size_t pos)
{
    size_t buf_pos = 0;
    char buf[BUF_SIZE] = { 0 };

    while (line[pos] != '\0' && (line[pos] == ' ' || line[pos] == '\t'))
        pos++;

    while (line[pos] != '\n' && line[pos] != '\0' && line[pos] != '#')
    {
        buf[buf_pos++] = line[pos++];
    }

    var->val = tab_to_char(buf, buf_pos);
    var->val_len = buf_pos;

    return 1;
}

void destroy_vars(struct var_list *var_list)
{
    for (size_t i = 0; i < var_list->size; i++)
    {
        free(var_list->list[i]->name);
        free(var_list->list[i]->val);
        free(var_list->list[i]);
    }

    free(var_list);
}