#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/makefile_parser.h"
#include "include/tools.h"

char *var_exists(char *line, struct var_list *var_list)
{
    int exists = 0;
    size_t i = 0;
    size_t buf_pos = 0;

    char buf[BUF_SIZE] = { 0 };

    while (line[i] != '\0' && line[i] != '$')
        i++;

    if (line[i] != '\0' && line[i] == '$' && line[i + 1] != '$')
    {
        size_t pos = i + 2;

        if (line[pos - 1] != '(' && line[pos - 1] != '{')
            buf[0] = line[pos - 1];

        else
        {
            for (pos = i + 2;
                 line[pos] != '\0' && line[pos] != ')' && line[pos] != '}';
                 pos++)
            {
                buf[buf_pos++] = line[pos];
            }
        }

        if (line[pos] == '\0')
            return NULL;

        for (size_t pos = 0; exists != 1 && pos < var_list->size; pos++)
        {
            if (!strcmp(buf, var_list->list[pos]->name))
            {
                line = replace_var_val(line, var_list->list[pos]->val,
                                       var_list->list[pos]->val_len, i);
            }
        }
    }

    return line;
}

char *tab_to_char(char tab[], size_t buf_size)
{
    if (buf_size == 0)
        return NULL;

    char *c = malloc(sizeof(char) * (buf_size + 1));

    if (c == NULL)
        return NULL;

    size_t i = 0;

    for (i = 0; tab[i] != '\0'; i++)
        c[i] = tab[i];

    c[i] = '\0';

    return c;
}

int is_recipe(char *line)
{
    if (line == NULL)
        return 0;

    if (line[0] != '\t')
        return 0;

    return 1;
}

int is_rule(char *line)
{
    if (line == NULL)
        return 0;

    size_t i = 0;

    if (line[0] == '\t')
        return 0;

    while (line[i] != '#' && line[i] != '\0' && line[i] != ':'
           && line[i] != '\n')
    {
        i++;
    }

    if (line[i] != ':')
        return 0;

    return 1;
}

int is_var(char *line)
{
    if (line == NULL)
        return 0;

    size_t i = 0;

    while (line[i] != '#' && line[i] != '\0' && line[i] != '\n'
           && line[i] != '=')
    {
        i++;
    }

    if (line[i] != '=')
        return 0;

    return 1;
}
