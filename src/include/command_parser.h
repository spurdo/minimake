#ifndef COMMAND_PARSER_H
#define COMMAND_PARSER_H

#define FILE_FLAG 1
#define PRINT_FLAG 1 << 1
#define HELP_FLAG 1 << 2

struct flag
{
    int flags;
    char **targets;
};

struct flag *get_flags(int argc, char **argv, char **targets, char *filename);

#endif