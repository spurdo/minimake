#ifndef MAKEFILE_PARSER_H
#define MAKEFILE_PARSER_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#define BUF_SIZE 4096

struct rule
{
    char *rule_name;

    char *deps[BUF_SIZE];
    char *recipes[BUF_SIZE];

    size_t deps_num;
    size_t recp_num;

    int has_dep;
    int has_recipe;
};

struct rule_list
{
    size_t size;
    struct rule *list[BUF_SIZE];
};

struct vars
{
    char *name;
    char *val;

    size_t val_len;
    size_t name_len;
};

struct var_list
{
    size_t size;
    struct vars *list[BUF_SIZE];
};

int parse(FILE *file, struct rule_list *rule_list, struct var_list *var_list,
          size_t bytes);

size_t set_rule_name(struct rule *rule, char *line);
int set_rule_dep(struct rule *rule, char *line, size_t pos);
int set_rule_recipe(struct rule *rule, FILE *file, char *line);

struct rule_list *init_list(FILE *file);
int init_rules(struct rule_list *rule_list);
void destroy_list(struct rule_list *rule_list);

size_t set_var_name(struct vars *var, char *line);
int set_var_val(struct vars *var, char *line, size_t pos);

void destroy_vars(struct var_list *var_list);
int init_var(struct var_list *var_list);
struct var_list *get_var_list(FILE *file);

#endif