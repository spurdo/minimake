#ifndef SHELL_H
#define SHELL_H

#include "command_parser.h"
#include "makefile_parser.h"

void process(struct rule *rule, struct var_list *var_list,
             struct rule_list *rule_list);
int start_exec(struct rule_list *rule_list, struct var_list *var_list,
               char **targets);
int exec_make(struct rule_list *rule_list, struct var_list *var_list,
              struct flag *flags, FILE *fp);

#endif