#ifndef TOOLS_H
#define TOOLS_H

#include <stddef.h>

#include "makefile_parser.h"

struct rule *get_rule(struct rule_list *list, char *name);
void print_targets(char **buf);
void print_help(void);
void print_vars(struct var_list *var_list);
void print_data(struct rule_list *rule_list);
void str_memset(char *buf[]);
char *replace_with_val(char *line, struct var_list *var_list);
char *var_exists(char *line, struct var_list *var_list);
char *replace_var_val(char *line, char *val, size_t val_len, size_t start);
char *tab_to_char(char tab[], size_t buf_index);
char *get_val(char *var, struct var_list *var_list);
int is_recipe(char *line);
int is_rule(char *line);
int is_var(char *line);
int get_file_pos(char **argv);

#endif