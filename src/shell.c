#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "include/command_parser.h"
#include "include/makefile_parser.h"
#include "include/tools.h"

void process(struct rule *rule, struct var_list *v, struct rule_list *r)
{
    int wait_res;

    for (size_t x = 0; x < rule->deps_num; x++)
    {
        struct rule *new_rule = get_rule(r, rule->deps[x]);
        if (new_rule != NULL)
            process(new_rule, v, r);
    }

    for (size_t i = 0; i < rule->recp_num; i++)
    {
        int pid = fork();

        if (pid == 0)
        {
            rule->recipes[i] = replace_with_val(rule->recipes[i], v);

            if (rule->recipes[i][0] == '@')
                rule->recipes[i][0] = ' ';

            else
            {
                printf("%s\n", rule->recipes[i]);
                fflush(stdout);
            }

            int res =
                execl("/bin/sh", "minimake", "-c", rule->recipes[i], NULL);

            if (res == -1)
                errx(EXIT_FAILURE, "Could not run execl");
        }

        else
        {
            if (waitpid(pid, &wait_res, 0) == -1)
                errx(EXIT_FAILURE, "Could not wait");
        }
    }
}

int start_exec(struct rule_list *rule_list, char **tar,
               struct var_list *var_list)
{
    size_t target_pos = 0;

    if (tar == NULL || tar[0] == NULL)
        process(rule_list->list[0], var_list, rule_list);

    else
    {
        while (tar[target_pos] != NULL)
        {
            int found = 0;

            for (size_t i = 0; i < rule_list->size; i++)
            {
                if (!strcmp(tar[target_pos], rule_list->list[i]->rule_name))
                {
                    found = 1;
                    process(rule_list->list[i], var_list, rule_list);
                    tar[target_pos] = NULL;
                    break;
                }
            }

            if (!found)
            {
                printf("minimake: *** No rule to make target '%s'. Stop.\n",
                       tar[target_pos]);
                return 1;
            }

            target_pos++;
        }
    }

    return 0;
}

int exec_make(struct rule_list *rule_list, struct var_list *var_list,
              struct flag *flags, FILE *fp)
{
    fseek(fp, 0, SEEK_SET);

    int parsed = parse(fp, rule_list, var_list, 0);

    if (parsed == 2)
        return 1;

    if (flags->flags & HELP_FLAG)
    {
        print_help();
        return 0;
    }

    if (flags->flags & PRINT_FLAG)
    {
        print_vars(var_list);
        print_data(rule_list);
        return 0;
    }

    int res = start_exec(rule_list, flags->targets, var_list);

    return res;
}
