CC = gcc
DEBUG =
CFLAGS = $(DEBUG) -D_POSIX_C_SOURCE=200809L -Wall -Wextra -Werror -std=c99 -pedantic
SRC = src/command_parser.c src/makefile_parser_rule_1.c src/makefile_parser_rule_2.c src/makefile_parser_var.c src/minimake.c src/shell.c src/tools_1.c src/tools_2.c src/tools_3.c
OBJ = ${SRC:.c=.o}
BIN = minimake
REMOVE = rm -rf

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(CFLAGS) $(SRC) -o $(BIN)

check:
	@echo No test suites have been implemented in this project.

clean:
	$(REMOVE) $(OBJ) $(BIN) a.out


